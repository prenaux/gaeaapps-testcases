=== WebAnimMixer (Level)
* n1 : idle
* n2 : walk
* n3 : run
* n4 : sprint
* n5 : jump
* a or left : turn counter clockwise the character
* d or right : turn clockwise the character