=== FactorySpawn (Level_FactorySpawn)
Input :
* 'n1' or 'space' : Spawn entity


Description :
Blue cube is the spawner. 
Press 'n1' or 'space' to spawn a cube that change color, white to red.